import { Component, OnInit } from '@angular/core';
import {CountryServices} from '../countries.service';
 
@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.scss']
})
export class RegionsComponent implements OnInit {

  regions = ['Europe', 'Asia']
  countries: any = ['--']
  isCountryDisabled:boolean = true;
  regionResponse: any
  countryDetails: any;
  displayCountryTable: boolean = false

  constructor( private service: CountryServices) { }

  ngOnInit(): void {
  }

  onRegionChange(event: any){
    if(event.target.value === "select"){
      this.isCountryDisabled = true;
      this.countries = ['--']
      this.displayCountryTable = false;     
    } else { 
      let selectedRegion =   event.target.value.toLowerCase();  
      this.service.getCountries(selectedRegion).subscribe( (res: any) => { 
      this.regionResponse = res
      this.countries = res.map( value => value.name);});
      this.isCountryDisabled = false;
      this.displayCountryTable = false;   
    }
  }

  onCountryChange(event: any){ 
    this.displayCountryTable = true;
    this.countryDetails = this.regionResponse.filter( val =>{
     if( val.name === event.target.value){
       return val;
     }
    });
  }
 
}
